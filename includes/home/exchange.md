## {{ exchange-heading[heading] The Acceptable Ads Exchange (AAX) }}

{{ exchange-1 A programmatic ad exchange that allows publishers and advertisers to access the Acceptable Ads inventory. }}

[{{ exchange-button[button text] Find out more }}](solutions){: .button .green }
