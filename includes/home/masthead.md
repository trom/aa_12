# {{ building-bridges[heading] Building bridges. }}

{{ building-bridges-1 Great things happen when people cooperate. }}

{{ building-bridges-2 Acceptable Ads allow publishers, advertisers, and ad-blocking users to work together. Working together creates a better, more sustainable ecosystem for everyone. }}

{{ building-bridges-3 Together we can make online advertising better. Join the Acceptable Ads initiative and reach new users. }}

[{{ read-more[button text] Read more }}](#advantages){: .button .green }
