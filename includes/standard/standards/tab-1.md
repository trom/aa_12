## {{standards-placement-heading[Standards placement heading] <span>01</span> Placement }}

{{standards-placement-body-text-1[Standards placement body text 1] Ads should not disrupt the natural reading flow. They should be placed above, beside or below the Primary Content<sup aria-labelledby="primary-content-tab">1</sup>.}}

<sup id="primary-content-tab">1</sup> The 'Primary Content' is defined as <a href="(https://developer.mozilla.org/en-US/docs/Web/HTML/Element/main">Mozilla's description</a> of the <code>&lt;main&gt;</code> HTML element): The Primary Content consists of content that is directly related to, or expands upon the central topic of a document or the central functionality of an application. This content should be unique to the document, excluding any content that is repeated across a set of documents such as sidebars, navigation links, copyright information, site logos, and search forms (unless, of course, the document's main function is a search form).
