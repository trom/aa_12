## For-profit Coalition

### Advertisers

Representative: {representative_name}, {representative_position}, {representative_compay}

Members:
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}

[Apply](#application-form)

### Ad Tech Agencies

Representative: {representative_name}, {representative_position}, {representative_compay}

Members:
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}

[Apply](#application-form)

### Advertising Agencies

Representative: {representative_name}, {representative_position}, {representative_compay}

Members:
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}

[Apply](#application-form)

### Publishers & Content Creators

Representative: [Apply](#application-form)

Members:
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}

[Apply](#application-form)

## User Advocate Coalition

### Digital Rights Organizations

Representatives:
- {representative_name}, {representative_position}, {representative_compay}
- {representative_name}, {representative_position}, {representative_compay}
- {representative_name}, {representative_position}, {representative_compay}

Members:
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}

[Apply](#application-form)

### User

Representative: {representative_name}, {representative_position}, {representative_compay}

Members:
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}

[Apply](#application-form)

## Expert Coalition

### Creative Agents

Representative: [Apply](#application-form)

Members:
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}

[Apply](#application-form)

### Researchers & Academics

Representative: {representative_name}, {representative_position}, {representative_compay}

Members:
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}
- {member_name}, {member_position}, {member_compay}

[Apply](#application-form)

### User Agents

Representative: {representative_name}, {representative_position}, {representative_compay}

Members:
[Apply](#application-form)
