# {{coalitions-column-1-heading[Coalitions columns-1 heading] Expert Coalition }}

{{coalitions-column-3-body-text[Coalitions column-3 body text] User agents, creative agents, researchers, and academics are in this group. They are stakeholders who are specialists in online advertising and ad blocking.}}
