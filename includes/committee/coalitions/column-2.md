# {{coalitions-column-1-heading[Coalitions columns-1 heading] User Advocate Coalition }}

{{coalitions-column-2-body-text[Coalitions column-2 body text] This coalition consists of stakeholders who are defenders of user choice. Digital rights organizations and ad-blocking users make up this group.}}
