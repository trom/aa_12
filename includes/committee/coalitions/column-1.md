# {{coalitions-column-1-heading[Coalitions column-1 heading] For-profit Coalition }}

{{coalitions-column-1-body-text[Coalitions column-1 body text] This coalition consists of business stakeholders, including advertisers, ad-tech providers, advertising agencies, publishers, and content creators.}}
