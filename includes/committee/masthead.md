# {{ masthead-heading-1[Masthead heading line 1] The Acceptable Ads Committee }}

{{ masthead-body1[Masthead body text] The independent Acceptable Ads Committee (AAC), established in 2017, creates exceptional ad standards. This improves user experience while delivering real value to content publishers and online advertisers. }}

{{ masthead-body2[Masthead body text] The AAC determines the criteria that define which ads are acceptable. It also governs the Acceptable Ads Standard. }}
