var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function () {
  return gulp.src('./static/scss/main.scss')
             .pipe(sass()
             .on('error', sass.logError))
             .pipe(gulp.dest('./static/css'));
});

gulp.task('watch', function () {
  gulp.watch('./static/scss/**/*.scss', ['sass']);
});

gulp.task('build', ['sass']);

gulp.task('default', ['build', 'watch']);
